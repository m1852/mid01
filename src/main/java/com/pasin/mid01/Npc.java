/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pasin.mid01;

/**
 *
 * @author Pla
 */
public class Npc {
    private String job="";
    private String name = "" ;
    private double hp ;
    private String walkType ="";
    private int walkSpeed;
    private char friendly =' ';
    public Npc ( String job ,String name ,double hp ,String walkType ,int walkSpeed,char friendly){
        this.job =job;
        this.name =name;
        this.hp =hp;
        this.walkType=walkType;
        this.walkSpeed=walkSpeed;
        this.friendly=friendly;
    }
    public String checkFriendly(char friendly){
        if(friendly == 'F'){
            return "Friend";
        }else if(friendly == 'E')
            return "Enemies";
        return null;
    }
    
    public void walk(){
        System.out.println(job +" "+ name+">> WalkType is == "+walkType+" : WalkSpeed is == "+walkSpeed );
    }
    public void showInfo(){
        System.out.println("Hello My Name is "+name+" I am "+job+" And I "+checkFriendly(friendly));
    }
    public void showHp(){
        System.out.println(job +" "+ name+">> Hp Left" + hp);
        System.out.println("===========================================================");
    }
}
