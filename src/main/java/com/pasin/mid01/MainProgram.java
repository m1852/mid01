/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pasin.mid01;

/**
 *
 * @author Pla
 */
public class MainProgram {
    public static void main(String[] args) {
        Npc npc01 =new Npc("Guide" ,"Steve",100.00,"Ground",50,'F');
        npc01.showInfo();
        npc01.walk();
        npc01.showHp();
        
        Npc npc02 =new Npc("Demon" ,"Harpy",125.00,"Fly",50,'E');
        npc02.showInfo();
        npc02.walk();
        npc02.showHp();
        
        Npc npc03 =new Npc("Wizard" ,"CJ",75.00,"Ground",25,'F');
        npc03.showInfo();
        npc03.walk();
        npc03.showHp();
    }
}
